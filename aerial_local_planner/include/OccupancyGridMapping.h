//
// Created by l4ncelot on 11/25/15.
//

#ifndef PROJECT_OCCUPANCYGRIDMAPPING_H
#define PROJECT_OCCUPANCYGRIDMAPPING_H


#include <ros/ros.h>
#include <nav_msgs/OccupancyGrid.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/PointCloud.h>
#include <geometry_msgs/Pose.h>
#include <tf/tf.h>
#include <tf/message_filter.h>
#include <laser_geometry/laser_geometry.h>
#include <message_filters/subscriber.h>

class OccupancyGridMapping {
public:

    OccupancyGridMapping(ros::NodeHandle &nh, const std::string &name_space);

    void init();

    void cb_laser_scan(const sensor_msgs::LaserScanConstPtr &msg);

    std::vector<double> compute_laser_edge(const double &angle, const double &range);

    void convert_to_tf_quaternion(const geometry_msgs::Quaternion &geometry_quaternion, tf::Quaternion &tf_quaternion);

    void cb_robot_pose(const geometry_msgs::PoseStampedConstPtr &msg);

    std::vector<int> convert_to_map_points(const std::vector<double> &data);

    void draw_point(const long &x, const long &y, const int &intensity);

    void line(const std::vector<double> &laser_edge);

    bool ready_to_map();

    void update_n(const long &x, const long &y, std::vector<int> &n, const int increment);

    int choose_color(const double &x, const double &y, const double &z);

    void draw_map();

    void create_map();

private:
    ros::NodeHandle nh;
    ros::Publisher pub_map, pub_zero_laser, pub_pcloud;
    ros::Subscriber sub_robot_pose;

    message_filters::Subscriber<sensor_msgs::LaserScan> sub_laser_scan;

    sensor_msgs::LaserScan laser_scan;

    nav_msgs::OccupancyGrid map;

    geometry_msgs::Pose robot_pose;
    std::vector<int> robot_pos;
    std::vector<int> n_free, n_occ, n_hid;

    tf::TransformListener listener;
    tf::MessageFilter<sensor_msgs::LaserScan> laser_notifier;
    laser_geometry::LaserProjection projector;

    sensor_msgs::PointCloud2 pcloud;

    std::string NAME_SPACE;
    double RESOLUTION{0.5};
    unsigned int WIDTH{200};
    unsigned int HEIGHT{200};
    double ALPHA{0.9};
};


#endif //PROJECT_OCCUPANCYGRIDMAPPING_H
