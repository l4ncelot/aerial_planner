/**
 * Author Michal Vatecha.
 * email: vatecmic@fel.cvut.cz
 */

#ifndef PROJECT_CARROTCHASE_H
#define PROJECT_CARROTCHASE_H

#include <ros/ros.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Twist.h>
#include <math.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf/message_filter.h>
#include <message_filters/subscriber.h>
#include <std_msgs/Bool.h>
#include <laser_geometry/laser_geometry.h>
#include <sensor_msgs/LaserScan.h>

#include <dynamic_reconfigure/server.h>
#include "aerial_local_planner/CarrotChaseConfig.h"

class carrot_chase {
public:

    /// \brief Constructor initializing basic data such as ROS subscribers and publishers
    /// \param [in] nh ROS nodehandle
    /// \param [in] name_space Namespace in which the node was run
    /// \return
    carrot_chase(ros::NodeHandle& nh, const std::string& name_space);

    /// \brief Initialize basic data to begin with
    void initialize();

    /// \brief Initialize transforms for waypoints 1 and 2
    void initializeTransforms();

    /// \brief Robot pose callback function saving current robot pose
    /// \param [in] message Message received from robot pose publisher
    void callbackRobotPose(const geometry_msgs::PoseStampedPtr& message);

    /// \brief Testing callback function to scan space around quadcopter
    /// \param [in] message Boolean message (1-do scan, 0-don't scan)
    void callbackScanSpace(const std_msgs::BoolConstPtr& message);

    /// \brief Callback function transformting laser scans to pointcloud and publishing them
    /// \param [in] message Incomming data from laser
    void callbackLaserScan(const sensor_msgs::LaserScanConstPtr& message);

    /// \brief Computes Euclidean distance between 2 points in 2D space
    /// \param [in] point1 Point1
    /// \param [in] point2 Point2
    /// \return 2D Euclidean distance between 2 points
    double compute2DEuclideanDistance(const geometry_msgs::Pose& point1, const geometry_msgs::Pose& point2);

    /// \brief Converts geometry_msgs Quaternion to tf Quaternion
    /// \param [in] geometry_quaternion geometry_msgs representation of quaternion
    /// \param [out] tf_quaternion tf_msgs representation of quaternion
    void geometry2TfQuaternion(const geometry_msgs::Quaternion& geometry_quaternion,
                               tf::Quaternion& tf_quaternion);

    /// \brief Limits angular part of velocity based on current speed and minimum turn radius
    /// \param [in] angular_velocity angular velocity of UAV
    /// \return limited angular velocity
    double angularVelocityLimit(const double& angular_velocity);

    /// \brief Returns the altitude difference between robot and it's waypoint
    /// \return altitude difference
    double altitudeDifferenceRobotWaypoint();

    /// \brief Determines whether altitude has ben reached or not
    /// \return altitude is reached
    bool isAltitudeReached();

    /// \brief Starts reaching given altitude in case altitude has not been reached yet
    void reachAltitude();

    /// \brief Stops the robot
    void stopRobot();

    /// \brief Determine if robot is ready to plan the movement towards it's waypoint
    /// \return is ready to plan movement
    bool isReadyToPlanMovement();

    /// \brief Stops climbing movement
    void stopClimbing();

    /// \brief Returns z part of angular velocity based on carrot chashing (CC) algorithm
    /// \return z part of angular velocity
    double computeCCAngularVelocity();

    /// \brief Moves the robot using velocities from CC algorithm
    void moveRobot();

    /// \brief Plans robot's movement based on CC algorithm with added vertical dimension
    void planRobotMovement();

    /// \brief Publishes cmd_velocity to given topic
    void publishCmdVelocity();

    /// \brief Broadcasts tf transformations of waypoints 1,2 and carrot
    void broadcastTransforms();

    /// \brief Initiate take off
    void takeOff();

    /// \brief Updates transform origins of waypoints 1 and 2
    void updateWaypointOrigins();

    /// \brief Callback function of robot's waypoint target
    /// \param [in] message waypoint pose
    void callbackWaypointTarget(const geometry_msgs::PoseConstPtr& message);

    /// \brief Callback function which determines the setting of new target
    /// \param [in] message bool message (1-target's changed, 0-target remains the same)
    void callbackWaypointChanged(const std_msgs::BoolConstPtr& message);

    /// \brief Setter for delta value
    /// \param [in] delta delta value
    void setDelta(double& delta);

private:
    ros::Publisher pub_cmd_velocity_, pub_pointcloud_, pub_pointcloud2_;
    ros::Subscriber sub_robot_pose_, sub_waypoint_target_, sub_waypoint_changed_;
    ros::Subscriber sub_scan_space_;

    geometry_msgs::Pose pose_waypoint1_, pose_waypoint2_, pose_robot_;
    geometry_msgs::Twist cmd_velocity_;

    laser_geometry::LaserProjection laser_projector_;
    tf::TransformListener tf_listener_;
    message_filters::Subscriber<sensor_msgs::LaserScan> sub_laser_scan_;

    tf::Transform tf_waypoint1_, tf_waypoint2_, tf_carrot_;

    tf::MessageFilter<sensor_msgs::LaserScan> laser_notifier_;


    // Constants
    const double MAX_VELOCITY{5.5};         /// maximum velocity in m/s

    const double K{0.2};                    /// proportional constant for limiting angular velocity
    double delta_{
            0.0};                      /// parameter influencing cross-track error (it's not constant due to debugging purposes)
    const double R_MIN{9.0};                /// minimum turning radius
    const double ALTITUDE_TOLERANCE{2.0};   /// consider altitude reached if the difference is less then this parameter

    double ground_speed_;                   /// ground speed of quadcopter

    bool target_arrived_, stop_publishing_velocity_;

    std::string name_space_, name_space_clean_;
    std::string world_frame_name_{"world"};
    std::string waypoint1_frame_name_{"/waypoint1"};
    std::string waypoint2_frame_name_{"/waypoint2"};
    std::string carrot_frame_name_{"/carrot"};
};


#endif //PROJECT_CARROTCHASE_H
