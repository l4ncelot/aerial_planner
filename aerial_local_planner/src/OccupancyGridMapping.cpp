//
// Created by l4ncelot on 11/25/15.
//

#include "OccupancyGridMapping.h"

OccupancyGridMapping::OccupancyGridMapping(ros::NodeHandle &nh, const std::string &name_space) :
        pub_map(nh.advertise<nav_msgs::OccupancyGrid>("/map", 10)),
        pub_zero_laser(nh.advertise<sensor_msgs::LaserScan>("/zero_laser", 10)),
        pub_pcloud(nh.advertise<sensor_msgs::PointCloud2>(name_space + "/pcloud", 10)),
        sub_laser_scan(nh, name_space + "/scan", 10),
        sub_robot_pose(
                nh.subscribe(name_space + "/ground_truth_to_tf/pose", 10, &OccupancyGridMapping::cb_robot_pose, this)),
        laser_notifier(sub_laser_scan, listener, "world", 10),
        NAME_SPACE(name_space) {

    init();
}


void OccupancyGridMapping::init() {
    map.info.resolution = (float) RESOLUTION;
    map.info.width = WIDTH;
    map.info.height = HEIGHT;
    map.data.resize(WIDTH * HEIGHT);
    n_free.resize(WIDTH * HEIGHT);
    n_occ.resize(WIDTH * HEIGHT);
    n_hid.resize(WIDTH * HEIGHT);
    map.info.origin.position.x = (-WIDTH * RESOLUTION) / 2.0;
    map.info.origin.position.y = (-HEIGHT * RESOLUTION) / 2.0;
    map.header.frame_id = "world";

    NAME_SPACE.erase(0, 1);

    laser_notifier.registerCallback(boost::bind(&OccupancyGridMapping::cb_laser_scan, this, _1));
    laser_notifier.setTolerance(ros::Duration(0.01));
}

void OccupancyGridMapping::cb_laser_scan(const sensor_msgs::LaserScanConstPtr &msg) {
    laser_scan = *msg;
//    filter_laser_scan(msg);

    try {
//        laser_projector_.transformLaserScanToPointCloud("world", *msg, pcloud, tf_listener_);
        projector.transformLaserScanToPointCloud(NAME_SPACE + "/laser0_frame", *msg, pcloud, listener);
        pub_pcloud.publish(pcloud);
    } catch (tf::TransformException &e) {
        ROS_WARN_STREAM(e.what());
        return;
    }
}

void OccupancyGridMapping::cb_robot_pose(const geometry_msgs::PoseStampedConstPtr &msg) {
    robot_pose = msg->pose;

    std::vector<double> rob_pos{robot_pose.position.x, robot_pose.position.y};
    robot_pos = convert_to_map_points(rob_pos);
}

void OccupancyGridMapping::convert_to_tf_quaternion(const geometry_msgs::Quaternion &geometry_quaternion,
                                                    tf::Quaternion &tf_quaternion) {
    tf_quaternion.setX(geometry_quaternion.x);
    tf_quaternion.setY(geometry_quaternion.y);
    tf_quaternion.setZ(geometry_quaternion.z);
    tf_quaternion.setW(geometry_quaternion.w);
}

std::vector<double> OccupancyGridMapping::compute_laser_edge(const double &angle, const double &range) {
    tf::Quaternion quaternion;
    convert_to_tf_quaternion(robot_pose.orientation, quaternion);
    double robot_yaw = tf::getYaw(quaternion);

    double x = range * cos(angle + robot_yaw) + robot_pose.position.x;
    double y = range * sin(angle + robot_yaw) + robot_pose.position.y;

    std::vector<double> vector{x, y};

    return vector;
}

std::vector<int> OccupancyGridMapping::convert_to_map_points(const std::vector<double> &data) {
    int x = (int) (round(data.at(0) / RESOLUTION + WIDTH / 2));
    int y = (int) (round(data.at(1) / RESOLUTION + HEIGHT / 2));

    std::vector<int> vector{x, y};

    return vector;
}

void OccupancyGridMapping::draw_point(const long &x, const long &y, const int &intensity) {
    unsigned long position = static_cast<unsigned long>(y * HEIGHT + x + 1);
    if (position < map.data.size()) {
        map.data.at(position) = intensity;
    }
}

void OccupancyGridMapping::update_n(const long &x, const long &y, std::vector<int> &n, const int increment) {
    unsigned long position = static_cast<unsigned long>(y * HEIGHT + x + 1);
    if (position < n.size()) {
        n.at(position) = n.at(position) + increment;
    }
}

void OccupancyGridMapping::line(const std::vector<double> &laser_edge) {
    std::vector<int> laser_pos = convert_to_map_points(laser_edge);

    int x0 = robot_pos.at(0);
    int y0 = robot_pos.at(1);
    int x1 = laser_pos.at(0);
    int y1 = laser_pos.at(1);

    double delta_x = x1 - x0;
    int sx = (delta_x < 0) ? -1 : 1;
    double delta_y = y1 - y0;
    int sy = (delta_y < 0) ? -1 : 1;

    double slope, pitch;

    if (fabs(delta_y) < fabs(delta_x)) {
        slope = delta_y / delta_x;
        pitch = y0 - slope * x0;

        int end = x0 + int(round(laser_scan.range_max / RESOLUTION)) * sx;
//        int end = x0 + int(round(laser_edge.at(0) / RESOLUTION)) * sx;
        // check bounds
        while (x0 != end) {
            long y = (long) (round(slope * x0 + pitch));
            if (sx > 0) {
                if (x0 < x1) {
                    update_n(x0, y, n_free, 1);
                } else if (x0 > x1) {
                    update_n(x0, y, n_hid, 1);
                } else {
                    update_n(x0, y, n_occ, 1);
                }
            } else {
                if (x0 > x1) {
                    update_n(x0, y, n_free, 1);
                } else if (x0 < x1) {
                    update_n(x0, y, n_hid, 1);
                } else {
                    update_n(x0, y, n_occ, 1);
                }
            }
//            draw_point(x0, y, 0);
            x0 += sx;
        }
    } else {
        slope = delta_x / delta_y;
        pitch = x0 - slope * y0;

        int end = y0 + int(round(laser_scan.range_max / RESOLUTION)) * sy;
        while (y0 != end) {
            long x = (long) (round(slope * y0 + pitch));
            if (sy > 0) {
                if (y0 < y1) {
                    update_n(x, y0, n_free, 1);
                } else if (y0 > y1) {
                    update_n(x, y0, n_hid, 1);
                } else {
                    update_n(x, y0, n_occ, 1);
                }
            } else {
                if (y0 > y1) {
                    update_n(x, y0, n_free, 1);
                } else if (y0 < y1) {
                    update_n(x, y0, n_hid, 1);
                } else {
                    update_n(x, y0, n_occ, 1);
                }
            }
//            draw_point(x, y0, 0);
            y0 += sy;
        }
    }


    // CLASSIC SLAM
//    if(fabs(delta_y) < fabs(delta_x)){
//        slope = delta_y / delta_x;
//        pitch = y0 - slope * x0;
//        while(x0 != x1){
//            long y = (long)(round(slope * x0 + pitch));
//            draw_point(x0, y, 0);
//            x0 += sx;
//        }
//    } else {
//        slope = delta_x / delta_y;
//        pitch = x0 - slope * y0;
//
//        while(y0 != y1){
//            long x = (long)(round(slope * y0 + pitch));
//            draw_point(x, y0, 0);
//            y0 += sy;
//        }
//    }

    draw_point(laser_pos.at(0), laser_pos.at(1), 100);
}

bool OccupancyGridMapping::ready_to_map() {
    return (laser_scan.ranges.size() != 0);
}

//void OccupancyGridMapping::update_probabilities() {
//    for (unsigned int i = 0; i < n_free.size(); i++) {
//        double n = n_free.at(i) + n_occ.at(i) + n_hid.at(i);
//        double alpha_n = pow(ALPHA, n);
//        double con = (1.0 / 3.0) * alpha_n;
//        p_free.at(i) = con + (1 - alpha_n) * (((double) n_free.at(i)) / (n));
//        p_occ.at(i) = con + (1 - alpha_n) * (((double) n_occ.at(i)) / (n));
//        p_hid.at(i) = con + (1 - alpha_n) * (((double) n_hid.at(i)) / (n));
//    }
//}

int OccupancyGridMapping::choose_color(const double &x, const double &y, const double &z) {
//    return (int) (y * 100.0);
    double max = std::max({x, y, z});
    if (max == x) {
        return 20;
    } else if (max == y) {
        return 100;
    } else {
        return -1;
    }
}

void OccupancyGridMapping::draw_map() {
//    MAKE SURE TO COMPUTE THIS ONLY FOR AREA AROUND ROBOT

    for (unsigned int i = 0; i < n_free.size(); i++) {
        int n = n_free.at(i) + n_occ.at(i) + n_hid.at(i);

        double alpha_n = pow(ALPHA, n);
        double con = (1.0 / 3.0) * alpha_n;
        double p_free = con + (1 - alpha_n) * (((double) n_free.at(i)) / (n));
        double p_occ = con + (1 - alpha_n) * (((double) n_occ.at(i)) / (n));
        double p_hid = con + (1 - alpha_n) * (((double) n_hid.at(i)) / (n));

        map.data.at(i) = choose_color(p_free, p_occ, p_hid);
//        map.data.at(i) = (int)round(p_occ.at(i) * 100.0);
    }
//    unsigned long num = 15338;
//    int n = n_free.at(num) + n_occ.at(num) + n_hid.at(num);
//    double alpha_n = pow(ALPHA, n);
//    double con = (1.0 / 3.0) * alpha_n;
//    double p_free = con + (1 - alpha_n) * (((double) n_free.at(num)) / (n));
//    double p_occ = con + (1 - alpha_n) * (((double) n_occ.at(num)) / (n));
//    double p_hid = con + (1 - alpha_n) * (((double) n_hid.at(num)) / (n));
//    ROS_INFO("n: %d", (n_occ.at(2000) + n_free.at(2000) + n_hid.at(2000)));
//    ROS_INFO("free|occ|hid|   %f | %f | %f", p_free, p_occ, p_hid);
//    map.data.at(num) = -50;
}

void OccupancyGridMapping::create_map() {
    if (!ready_to_map()) return;

//    for (unsigned int i = 0; i < pcloud.points.size(); i++) {
//        if (laser_scan.ranges.at(i) < 0.1) continue;
////        std::vector<double> laser_edge = compute_laser_edge(angle, laser_scan.ranges.at(i));
//        std::vector<double> laser_edge{pcloud.points.at(i).x, pcloud.points.at(i).y};
//        line(laser_edge);
////        angle -= laser_scan.angle_increment;
//    }

//    update_probabilities();
//    draw_map();

//    pub_map.publish(map);
}