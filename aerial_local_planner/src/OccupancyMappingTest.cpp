//
// Created by l4ncelot on 11/25/15.
//

#include "OccupancyGridMapping.h"
#include <topic_tools/MuxSelect.h>

int main(int argc, char *argv[]){
    ros::init(argc, argv, "occupancy_mapping");
    ros::NodeHandle nh;

    const std::string &name_space = ros::this_node::getNamespace();
    OccupancyGridMapping mapping(nh, name_space);

    // TAKE OFF
    ros::Publisher pub = nh.advertise<geometry_msgs::Twist>(name_space + "/cmd_vel", 10);
    geometry_msgs::Twist g_message;
    g_message.linear.z = 2.0;

    int n = 0;
    while(n <= 10){
        pub.publish(g_message);
        ros::Duration(0.1).sleep();
        n++;
    }
    g_message.linear.z = 0.0;
    pub.publish(g_message);

//    ros::Rate r(1);
    while(ros::ok()){
        ros::spinOnce();
    }

    return 0;
}