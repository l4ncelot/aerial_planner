//
// Created by l4ncelot on 9/24/15.
//

#include <ros/ros.h>
#include <ompl/control/SpaceInformation.h>
#include <ompl/base/spaces/SE2StateSpace.h>
#include <ompl/control/ODESolver.h>
#include <ompl/control/spaces/RealVectorControlSpace.h>
#include <ompl/control/SimpleSetup.h>
#include <ompl/config.h>
#include <ompl/control/planners/rrt/RRT.h>
#include <ompl/control/planners/kpiece/KPIECE1.h>
#include <vector>
#include <iostream>
#include <valarray>
#include <limits>
#include <string>
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/Pose.h>

#include <tf/transform_broadcaster.h>

namespace ob = ompl::base;
namespace oc = ompl::control;

// Kinematic car model object definition.  This class does NOT use ODESolver to propagate the system.
class KinematicCarModel : public oc::StatePropagator {
public:
    KinematicCarModel(const oc::SpaceInformationPtr &si) : oc::StatePropagator(si) {
        space_ = si->getStateSpace();
        carLength_ = 0.1;
        timeStep_ = 0.01;
    }

    virtual void propagate(const ob::State *state, const oc::Control *control, const double duration,
                           ob::State *result) const {
        EulerIntegration(state, control, duration, result);
    }

protected:
    // Explicit Euler Method for numerical integration.
    void EulerIntegration(const ob::State *start, const oc::Control *control, const double duration,
                          ob::State *result) const {
        double t = timeStep_;
        std::valarray<double> dstate;
        space_->copyState(result, start);
        while (t < duration + std::numeric_limits<double>::epsilon()) {
            ode(result, control, dstate);
            update(result, timeStep_ * dstate);
            t += timeStep_;
        }
        if (t + std::numeric_limits<double>::epsilon() > duration) {
            ode(result, control, dstate);
            update(result, (t - duration) * dstate);
        }
    }

    void ode(const ob::State *state, const oc::Control *control, std::valarray<double> &dstate) const {
        const double *u = control->as<oc::RealVectorControlSpace::ControlType>()->values;
        const double theta = state->as<ob::SE2StateSpace::StateType>()->getYaw();

        dstate.resize(3);
        dstate[0] = u[0] * cos(theta);
        dstate[1] = u[0] * sin(theta);
        dstate[2] = u[0] * tan(u[1]) / carLength_;
    }

    void update(ob::State *state, const std::valarray<double> &dstate) const {
        ob::SE2StateSpace::StateType &s = *state->as<ob::SE2StateSpace::StateType>();
        s.setX(s.getX() + dstate[0]);
        s.setY(s.getY() + dstate[1]);
        s.setYaw(s.getYaw() + dstate[2]);
        space_->enforceBounds(state);
    }


    ob::StateSpacePtr space_;
    double carLength_;
    double timeStep_;

};

// Definition of the ODE for the kinematic car.
// This method is analogous to the above KinematicCarModel::ode function.
void KinematicCarODE(const oc::ODESolver::StateType &q, const oc::Control *control, oc::ODESolver::StateType &qdot) {
    const double *u = control->as<oc::RealVectorControlSpace::ControlType>()->values;
    const double theta = q[2];
    double carLength = 0.2;

    // Zero out qdot
    qdot.resize(q.size(), 0);

    qdot[0] = u[0] * cos(theta);
    qdot[1] = u[0] * sin(theta);
    qdot[2] = u[0] * tan(u[1]) / carLength;
}

// This is a callback method invoked after numerical integration.
void KinematicCarPostIntegration(const ob::State * /*state*/, const oc::Control * /*control*/,
                                 const double /*duration*/, ob::State *result) {
    // Normalize orientation between 0 and 2*pi
    ob::SO2StateSpace SO2;
    SO2.enforceBounds(result->as<ob::SE2StateSpace::StateType>()->as<ob::SO2StateSpace::StateType>(1));
}

bool isStateValid(const oc::SpaceInformation *si, const ob::State *state) {
    //    ob::ScopedState<ob::SE2StateSpace>
    const ob::SE2StateSpace::StateType *se2state = state->as<ob::SE2StateSpace::StateType>();

    const ob::RealVectorStateSpace::StateType *pos = se2state->as<ob::RealVectorStateSpace::StateType>(0);

    const ob::SO2StateSpace::StateType *rot = se2state->as<ob::SO2StateSpace::StateType>(1);



    // return a value that is always true but uses the two variables we define, so we avoid compiler warnings
    return si->satisfiesBounds(state) && (const void *) rot != (const void *) pos;
}

class DemoControlSpace : public oc::RealVectorControlSpace {
public:

    DemoControlSpace(const ob::StateSpacePtr &stateSpace) : oc::RealVectorControlSpace(stateSpace, 2) {
    }
};

void print_array(std::vector<std::vector<double> > &array) {
    ROS_WARN_STREAM("Printing velocities");
    for (int i = 0; i < array.size(); i++) {
        std::string message;
        message = boost::lexical_cast<std::string>(array.at(i).at(0)) + " | "
                  + boost::lexical_cast<std::string>(array.at(i).at(1)) + " | "
                  + boost::lexical_cast<std::string>(array.at(i).at(2)) + "\n";
        ROS_INFO_STREAM(message);
    }
}

nav_msgs::Path path;

std::vector<geometry_msgs::Twist> velocities;
std::vector<double> control_durations;

void set_velocities(const std::vector<oc::Control *> controls) {
    velocities.resize(controls.size());
    for (int i = 0; i < controls.size(); i++) {
        double *values = controls.at(i)->as<ompl::control::RealVectorControlSpace::ControlType>()->values;

        geometry_msgs::Twist velocity;
        velocity.linear.x = values[0];
        velocity.angular.z = values[1];
        velocities.at(i) = velocity;
    }
}

void set_path(const std::vector<ob::State *> &states){
    std::vector<geometry_msgs::PoseStamped> poses;
    poses.resize(states.size());

    std_msgs::Header header;
    header.frame_id = "odom";

    for(int i = 0; i < states.size(); i++){
        geometry_msgs::PoseStamped pose;
//        ob::SE2StateSpace::StateType stateType = *states.at(i)->as<ob::SE2StateSpace::StateType>();
        pose.pose.position.x = states.at(i)->as<ob::SE2StateSpace::StateType>()->getX();
        pose.pose.position.y = states.at(i)->as<ob::SE2StateSpace::StateType>()->getY();
        pose.header = header;
        poses.at(i) = pose;
    }
    path.header = header;
    path.poses = poses;
}

void planWithSimpleSetup(void) {
    ob::StateSpacePtr space(new ob::SE2StateSpace());

    ob::RealVectorBounds bounds(2);
    bounds.setLow(-50.0);
    bounds.setHigh(50.0);

    space->as<ob::SE2StateSpace>()->setBounds(bounds);

    // create a control space
    oc::ControlSpacePtr cspace(new DemoControlSpace(space));

    // set the bounds for the control space
    ob::RealVectorBounds cbounds(2);
    cbounds.setLow(-0.5);
    cbounds.setHigh(0.5);

    cspace->as<DemoControlSpace>()->setBounds(cbounds);

    // define a simple setup class
    oc::SimpleSetup ss(cspace);

//    ss.setPlanner(ob::PlannerPtr(new oc::RRT(ss.getSpaceInformation())));
    ss.setPlanner(ob::PlannerPtr(new oc::KPIECE1(ss.getSpaceInformation())));

    // set state validity checking for this space
    ss.setStateValidityChecker(boost::bind(&isStateValid, ss.getSpaceInformation().get(), _1));

    // Setting the propagation routine for this space:
    // KinematicCarModel does NOT use ODESolver
//    ss.setStatePropagator(oc::StatePropagatorPtr(new KinematicCarModel(ss.getSpaceInformation())));

    // Use the ODESolver to propagate the system.  Call KinematicCarPostIntegration
    // when integration has finished to normalize the orientation values.
    oc::ODESolverPtr odeSolver(new oc::ODEBasicSolver<> (ss.getSpaceInformation(), &KinematicCarODE));
    ss.setStatePropagator(oc::ODESolver::getStatePropagator(odeSolver, &KinematicCarPostIntegration));

    ob::ScopedState <ob::SE2StateSpace> start(space);
    start->setX(0.0);
    start->setY(0.0);
    start->setYaw(0.0);

    ob::ScopedState <ob::SE2StateSpace> goal(space);
    goal->setX(10.0);
    goal->setY(10.0);
    goal->setYaw(0.0);

    ss.setStartAndGoalStates(start, goal, 0.01);

    ss.setup();

    ob::PlannerStatus solved = ss.solve(10.0);

    if (solved) {
        std::cout << "Found solution:" << std::endl;

        ompl::geometric::PathGeometric pathGeometric = ss.getSolutionPath().asGeometric();
        pathGeometric.printAsMatrix(std::cout);

//        std::vector<ompl::base::State *> states = pathGeometric.getStates();
        std::vector<ompl::base::State *> states = ss.getSolutionPath().getStates();
        set_path(states);

        std::vector<ompl::control::Control *> controls = ss.getSolutionPath().getControls();
        control_durations = ss.getSolutionPath().getControlDurations();

        set_velocities(controls);

//        cmds.resize(controls.size());
//        for (int i = 0; i < controls.size(); i++) {
//            double *values = controls.at(i)->as<ompl::control::RealVectorControlSpace::ControlType>()->values;
//
//            geometry_msgs::Twist velocity;
//            velocity.linear.x = values[0];
//            velocity.angular.z = values[1];
//            cmds.at(i) = velocity;
//        }

//        pathGeometric.printAsMatrix(std::cout);
//        ss.getSolutionPath().asGeometric().printAsMatrix(std::cout);
        std::cout << "number of states: " + boost::lexical_cast<std::string>(ss.getSolutionPath().getStateCount()) << std::endl;
    }
    else
        std::cout << "No solution found" << std::endl;
}

int main(int argc, char *argv[]) {
    ros::init(argc, argv, "velocity_publisher");
    ros::NodeHandle nh;

    std::cout << "OMPL version: " << OMPL_VERSION << std::endl;
    ros::Publisher cmd_publisher = nh.advertise<geometry_msgs::Twist>("/cmd_vel", 1000);
    ros::Publisher path_publisher = nh.advertise<nav_msgs::Path>("/path", 1000);

    // TAKE OFF
//    int count = 0;
//    ROS_INFO_STREAM("engaging motors...");
//    geometry_msgs::Twist cmd;
//    cmd.linear.z = 1.0;
//    while (count < 19) {
//        cmd_publisher.publish(cmd);
//        ros::Duration(0.1).sleep();
//        count++;
//    }
//    cmd.linear.z = 0.0;
//    ROS_INFO_STREAM("stopping...");
//    cmd_publisher.publish(cmd);
//    ros::Duration(0.5).sleep();

    planWithSimpleSetup();


    ROS_WARN_STREAM("publishing path...");
//    while(ros::ok()){
//        path_publisher.publish(path);
//    }

    ROS_WARN_STREAM("SIZE: " + boost::lexical_cast<std::string>(velocities.size()));

    for (int i = 0; i < velocities.size(); i++) {
        cmd_publisher.publish(velocities.at(i));
        path_publisher.publish(path);
//        ROS_INFO_STREAM("[" + boost::lexical_cast<std::string>(command.linear.x)
//                        + " | " + boost::lexical_cast<std::string>(command.linear.y)
//                        + "]"
//        );
        ROS_INFO_STREAM("duration: " + boost::lexical_cast<std::string>(control_durations.at(i)));
        ros::Duration(control_durations.at(i)).sleep();
    }

    ros::Duration(0.5).sleep();
    geometry_msgs::Twist stop;

    cmd_publisher.publish(stop);


    while(ros::ok()){
        path_publisher.publish(path);
    }

    return 0;
}
