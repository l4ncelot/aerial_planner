/**
 * Author Michal Vatecha.
 * email: vatecmic@fel.cvut.cz
 */

/**
 * this is only testing class
 */

#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <std_msgs/String.h>
#include <carrot_chase.h>

class TfBroadcaster {
public:
    TfBroadcaster(ros::NodeHandle &nh) :
            cmd_publisher(nh.advertise<geometry_msgs::Twist>("/cmd_vel", 10)),
            robot_pose_subscriber(
                    nh.subscribe("/ground_truth_to_tf/pose", 10, &TfBroadcaster::estimate_robot_pose, this)) {

        init();
    }


    void init() {
        cmd.linear.x = 0.0, cmd.linear.y = 0.0, cmd.linear.z = 1.0;
        cmd.angular.x = 0.0, cmd.angular.y = 0.0, cmd.angular.z = 0.0;
        z_coordinate = 0.0;
        init_transform();
    }

    void init_transform() {
        update_transform_coordinates();
        tf::Quaternion quaternion;
        quaternion.setRPY(0, 0, 1);
        transform.setRotation(quaternion);
        transform1.setRotation(quaternion);
    }

    void update_transform_coordinates() {
        transform.setOrigin(tf::Vector3(0.0, 0.0, z_coordinate));
        transform1.setOrigin(tf::Vector3(10.0, 10.0, z_coordinate));
    }

    void take_off() {
        int count = 0;
        ROS_INFO_STREAM("engaging motors...");
        while (count < 19) {
            cmd_publisher.publish(cmd);
            ros::Duration(0.1).sleep();
            count++;
        }
        cmd.linear.z = 0.0;
        ROS_INFO_STREAM("stopping...");
        cmd_publisher.publish(cmd);
        ros::Duration(0.5).sleep();
    }

    void do_stuff() {
        take_off();
        ROS_INFO_STREAM("broadcasting...");
        while (ros::ok()) {
            update_transform_coordinates();
            broadcast_transform();
            ros::spinOnce();
        }
    }

    void broadcast_transform() {
        static tf::TransformBroadcaster br;
        ros::Time now = ros::Time::now();

        br.sendTransform(tf::StampedTransform(transform, now, "world", "W1"));
        br.sendTransform(tf::StampedTransform(transform1, now, "world", "W2"));
    }

private:
    void estimate_robot_pose(const geometry_msgs::PoseStampedPtr &msg) {
        z_coordinate = msg->pose.position.z;
    }

    double z_coordinate;

    ros::Publisher cmd_publisher;
    ros::Subscriber robot_pose_subscriber;

    geometry_msgs::Twist cmd;

    tf::Transform transform, transform1;

};

int main(int argc, char *argv[]) {
    ROS_INFO_STREAM("begin");
    ros::init(argc, argv, "tf_broadcaster");

    ROS_INFO_STREAM("init successful");
    ros::NodeHandle nh;
    ROS_INFO_STREAM("nodehandle created");

    TfBroadcaster broadcaster(nh);
    broadcaster.do_stuff();


    return 0;

}


