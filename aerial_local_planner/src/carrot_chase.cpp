/**
 * Author Michal Vatecha.
 * email: vatecmic@fel.cvut.cz
 */

#include <tf/transform_broadcaster.h>
#include "carrot_chase.h"

carrot_chase::carrot_chase(ros::NodeHandle& nh, const std::string& name_space) :
        pub_cmd_velocity_(nh.advertise<geometry_msgs::Twist>("/cmd_vel", 10)),
        pub_pointcloud_(nh.advertise<sensor_msgs::PointCloud>("/pcloud1", 10)),
        pub_pointcloud2_(nh.advertise<sensor_msgs::PointCloud2>("/pcloud", 10)),
        sub_robot_pose_(nh.subscribe(name_space + "/ground_truth_to_tf/pose", 10, &carrot_chase::callbackRobotPose, this)),
        sub_waypoint_target_(
                nh.subscribe(name_space + "/target_waypoint", 10, &carrot_chase::callbackWaypointTarget, this)),
        sub_waypoint_changed_(
                nh.subscribe(name_space + "/change_waypoint", 10, &carrot_chase::callbackWaypointChanged, this)),
        sub_laser_scan_(nh, name_space + "/scan", 10),
        name_space_(name_space),
        name_space_clean_(name_space.substr(1, name_space.length())),
        laser_notifier_(sub_laser_scan_, tf_listener_, world_frame_name_, 10),
        sub_scan_space_(nh.subscribe(name_space + "/scan_space", 10, &carrot_chase::callbackScanSpace, this)) {
    initialize();
}


void carrot_chase::initialize() {
    laser_notifier_.registerCallback(boost::bind(&carrot_chase::callbackLaserScan, this, _1));
    laser_notifier_.setTolerance(ros::Duration(0.01));


    pose_waypoint1_ = pose_robot_;
    pose_waypoint2_ = pose_robot_;
    pose_waypoint2_.position.z = 9.3;

    initializeTransforms();

    target_arrived_ = false;
    stop_publishing_velocity_ = true;

    cmd_velocity_.linear.z = 3.0;           // takeoff velocity
}

void carrot_chase::initializeTransforms() {
    updateWaypointOrigins();
    tf::Quaternion quaternion;

    quaternion.setRPY(0, 0, 0);
    tf_waypoint1_.setRotation(quaternion);
    tf_waypoint2_.setRotation(quaternion);
    tf_carrot_.setRotation(quaternion);
}

void carrot_chase::callbackRobotPose(const geometry_msgs::PoseStampedPtr& message) {
    pose_robot_ = message->pose;
}

void carrot_chase::callbackScanSpace(const std_msgs::BoolConstPtr& message) {
    if (!message) {
        ROS_INFO_STREAM("null message");
        return;
    }
    pose_waypoint2_.position = pose_robot_.position;
    if (message->data) {
        pose_waypoint2_.position.z = 0.7;

    } else {
        pose_waypoint2_.position.z = 10.8;
    }
}

void carrot_chase::callbackLaserScan(const sensor_msgs::LaserScanConstPtr& message) {
    sensor_msgs::PointCloud pcloud;
    sensor_msgs::PointCloud2 pcloud2;
    try {
        laser_projector_.transformLaserScanToPointCloud(world_frame_name_, *message, pcloud, tf_listener_);
        laser_projector_.transformLaserScanToPointCloud(world_frame_name_, *message, pcloud2, tf_listener_);
    } catch (tf::TransformException& e) {
        ROS_WARN_STREAM(e.what());
        return;
    }

    pub_pointcloud_.publish(pcloud);
    pub_pointcloud2_.publish(pcloud2);
}

double carrot_chase::compute2DEuclideanDistance(const geometry_msgs::Pose& point1, const geometry_msgs::Pose& point2) {
    geometry_msgs::Pose::_position_type position1 = point1.position;
    geometry_msgs::Pose::_position_type position2 = point2.position;
    return sqrt(pow(position1.x - position2.x, 2) + pow(position1.y - position2.y, 2));
}

void carrot_chase::geometry2TfQuaternion(const geometry_msgs::Quaternion& geometry_quaternion,
                                        tf::Quaternion& tf_quaternion) {
    tf_quaternion.setX(geometry_quaternion.x);
    tf_quaternion.setY(geometry_quaternion.y);
    tf_quaternion.setZ(geometry_quaternion.z);
    tf_quaternion.setW(geometry_quaternion.w);
}

double carrot_chase::angularVelocityLimit(const double& angular_velocity) {
    double constant = pow(ground_speed_, 2) / R_MIN;
    if (angular_velocity < 0 && fabs(angular_velocity) > constant) {
        return constant;
    }
    if (angular_velocity > 0 && fabs(angular_velocity) > constant) {
        return -constant;
    }
    return angular_velocity;
}

double carrot_chase::altitudeDifferenceRobotWaypoint() {
    return (pose_waypoint2_.position.z - pose_robot_.position.z);
}

bool carrot_chase::isAltitudeReached() {
    return (fabs(altitudeDifferenceRobotWaypoint()) < ALTITUDE_TOLERANCE);
}

void carrot_chase::reachAltitude() {
    double altitude_diff = altitudeDifferenceRobotWaypoint();
    double z_direction = (altitude_diff > 0) ? 1.0 : -1.0;

    cmd_velocity_.linear.z = MAX_VELOCITY * z_direction / 11.0;
//    ROS_INFO_STREAM("reaching altitude");
//    ROS_INFO("angular velocity: %f", cmd_velocity_.angular.z);
    broadcastTransforms();
    publishCmdVelocity();
}

void carrot_chase::stopRobot() {
    cmd_velocity_ = geometry_msgs::Twist();
    ros::Duration(0.5).sleep();
}


bool carrot_chase::isReadyToPlanMovement() {
//    if (!target_arrived_) {
////        ROS_INFO_STREAM("target didn't arrive");
//        return false;
//    }

    if (!isAltitudeReached()) {
        stopRobot();
        reachAltitude();
        return false;
    }

    return true;
}

void carrot_chase::stopClimbing() {
    cmd_velocity_.linear.z = 0.0;
}

double carrot_chase::computeCCAngularVelocity() {
    // Distance between robot and waypoint1
    double Ru = compute2DEuclideanDistance(pose_robot_, pose_waypoint1_);
    // Angle between line connecting waypoints 1-2 and x-axis
    double theta = atan2(pose_waypoint2_.position.y - pose_waypoint1_.position.y,
                         pose_waypoint2_.position.x - pose_waypoint1_.position.x);
    // Angle between line connecting waypoint1 with robot position and x-axis
    double theta_u = atan2(pose_robot_.position.y - pose_waypoint1_.position.y,
                           pose_robot_.position.x - pose_waypoint1_.position.x);
    double beta = theta - theta_u;

    // Distance between waypoint w1 and projection of robot on line connecting waypoint1 and waypoint2
    double R = sqrt(pow(Ru, 2) - pow(Ru * sin(beta), 2));

    // Coordniates of virtual "carrot"
    double carrot_x, carrot_y;
    carrot_x = (R + delta_) * cos(theta) + pose_waypoint1_.position.x;
    carrot_y = (R + delta_) * sin(theta) + pose_waypoint1_.position.y;

    tf_carrot_.setOrigin(tf::Vector3(carrot_x, carrot_y, pose_robot_.position.z));

    // Angle between direction of robot and connection of carrot point with robot
    double xi_d = atan2(carrot_y - pose_robot_.position.y, carrot_x - pose_robot_.position.x);

    tf::Quaternion quaternion;
    geometry2TfQuaternion(pose_robot_.orientation, quaternion);

    // Direction angle of robot
    double xi = tf::getYaw(quaternion);

    // Change of robot's direction angle
    double xi_dot = K * (xi_d - xi);

    // x and y vectors of robot's velocity
    double velocity_x, velocity_y;
    velocity_x = MAX_VELOCITY * cos(xi);
    velocity_y = MAX_VELOCITY * sin(xi);
    // velocity vector length
    ground_speed_ = sqrt(pow(velocity_x, 2) + pow(velocity_y, 2));

    return angularVelocityLimit(xi_dot * MAX_VELOCITY);
}

void carrot_chase::moveRobot() {
    cmd_velocity_.linear.x = MAX_VELOCITY;
    cmd_velocity_.angular.z = computeCCAngularVelocity();
//    ROS_INFO("angular velocity: %f", cmd_velocity_.angular.z);
}

void carrot_chase::planRobotMovement() {
    if (!isReadyToPlanMovement()) {
        stop_publishing_velocity_ = false;
        return;
    }

    if (!stop_publishing_velocity_) {
        stopClimbing();
        publishCmdVelocity();
        stop_publishing_velocity_ = true;
    }
//    moveRobot();
}

void carrot_chase::publishCmdVelocity() {
    pub_cmd_velocity_.publish(cmd_velocity_);
}

void carrot_chase::broadcastTransforms() {
    static tf::TransformBroadcaster br;
    ros::Time now = ros::Time::now();

    br.sendTransform(tf::StampedTransform(tf_waypoint1_, now, world_frame_name_, name_space_clean_ + waypoint1_frame_name_));
    br.sendTransform(tf::StampedTransform(tf_waypoint2_, now, world_frame_name_, name_space_clean_ + waypoint2_frame_name_));
    br.sendTransform(tf::StampedTransform(tf_carrot_, now, world_frame_name_, name_space_clean_ + carrot_frame_name_));
}

void carrot_chase::takeOff() {
    int count = 0;
    ROS_INFO_STREAM("engaging motors...");
    while (count < 33) {
        publishCmdVelocity();
        ros::Duration(0.1).sleep();
        count++;
    }
    ROS_INFO_STREAM("stopping...");
    stopClimbing();
    publishCmdVelocity();
    ros::Duration(0.5).sleep();
}


void carrot_chase::updateWaypointOrigins() {
    tf_waypoint1_.setOrigin(tf::Vector3(pose_waypoint1_.position.x, pose_waypoint1_.position.y, pose_waypoint1_.position.z));
    tf_waypoint2_.setOrigin(tf::Vector3(pose_waypoint2_.position.x, pose_waypoint2_.position.y, pose_waypoint2_.position.z));
}

void carrot_chase::callbackWaypointTarget(const geometry_msgs::PoseConstPtr& message) {
    if (!target_arrived_) {
        target_arrived_ = true;
        ROS_INFO_STREAM("target arrived");
    }
    pose_waypoint2_ = *message;
    updateWaypointOrigins();
}

void carrot_chase::callbackWaypointChanged(const std_msgs::BoolConstPtr& message) {
    if (message->data) {
        pose_waypoint1_ = pose_robot_;
    }
}

void carrot_chase::setDelta(double& delta) {
    delta_ = delta;
}