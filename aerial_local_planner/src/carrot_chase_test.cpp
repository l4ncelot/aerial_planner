/**
 * Author Michal Vatecha.
 * email: vatecmic@fel.cvut.cz
 */
#include <ros/ros.h>
#include <carrot_chase.h>

/**
 * Simple example of carrot chasing algorithm used as local planner for flying towards the target
 */

void dynamic_callback(aerial_local_planner::CarrotChaseConfig &config, uint32_t level, carrot_chase *carrotChase){
    carrotChase->setDelta(config.DELTA);
}

int main(int argc, char *argv[]){
    ros::init(argc, argv, "carrot_chase");
    ros::NodeHandle nh;

    ros::Duration(4.0).sleep();

    dynamic_reconfigure::Server<aerial_local_planner::CarrotChaseConfig> server;
    dynamic_reconfigure::Server<aerial_local_planner::CarrotChaseConfig>::CallbackType f;

    carrot_chase carrotChase(nh, ros::this_node::getNamespace());

    f = boost::bind(&dynamic_callback, _1, _2, &carrotChase);
    server.setCallback(f);

    ROS_INFO_STREAM("nodehandle initialized");
    carrotChase.takeOff();
    ROS_WARN_STREAM("starting planning...");
    while(ros::ok()){
        carrotChase.broadcastTransforms();
        carrotChase.planRobotMovement();
//        carrotChase.publishCmdVelocity();
        ros::Duration(0.01).sleep();
        ros::spinOnce();
    }

    return 0;

}
