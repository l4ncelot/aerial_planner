#include <ros/ros.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/Twist.h>

#include <ompl/control/SpaceInformation.h>
#include <ompl/base/goals/GoalState.h>
#include <ompl/base/spaces/SE2StateSpace.h>
#include <ompl/control/spaces/RealVectorControlSpace.h>
#include <ompl/control/planners/kpiece/KPIECE1.h>
#include <ompl/control/planners/rrt/RRT.h>
#include <ompl/control/planners/est/EST.h>
#include <ompl/control/planners/syclop/SyclopRRT.h>
#include <ompl/control/planners/syclop/SyclopEST.h>
#include <ompl/control/planners/pdst/PDST.h>
#include <ompl/control/planners/syclop/GridDecomposition.h>
#include <ompl/control/SimpleSetup.h>
#include <ompl/config.h>
#include <iostream>

namespace ob = ompl::base;
namespace oc = ompl::control;

// a decomposition is only needed for SyclopRRT and SyclopEST
class MyDecomposition : public oc::GridDecomposition {
public:
    MyDecomposition(const int length, const ob::RealVectorBounds &bounds)
            : GridDecomposition(length, 2, bounds) {
    }

    virtual void project(const ob::State *s, std::vector<double> &coord) const {
        coord.resize(2);
        coord[0] = s->as<ob::SE2StateSpace::StateType>()->getX();
        coord[1] = s->as<ob::SE2StateSpace::StateType>()->getY();
    }

    virtual void sampleFullState(const ob::StateSamplerPtr &sampler, const std::vector<double> &coord,
                                 ob::State *s) const {
        sampler->sampleUniform(s);
        s->as<ob::SE2StateSpace::StateType>()->setXY(coord[0], coord[1]);
    }
};

bool isStateValid(const oc::SpaceInformation *si, const ob::State *state) {
    //    ob::ScopedState<ob::SE2StateSpace>
    // cast the abstract state type to the type we expect
    const ob::SE2StateSpace::StateType *se2state = state->as<ob::SE2StateSpace::StateType>();

    // extract the first component of the state and cast it to what we expect
    const ob::RealVectorStateSpace::StateType *pos = se2state->as<ob::RealVectorStateSpace::StateType>(0);

    // extract the second component of the state and cast it to what we expect
    const ob::SO2StateSpace::StateType *rot = se2state->as<ob::SO2StateSpace::StateType>(1);

    // check validity of state defined by pos & rot


    // return a value that is always true but uses the two variables we define, so we avoid compiler warnings
    return si->satisfiesBounds(state) && (const void *) rot != (const void *) pos;
}

void propagate(const ob::State *start, const oc::Control *control, const double duration, ob::State *result) {
    const ob::SE2StateSpace::StateType *se2state = start->as<ob::SE2StateSpace::StateType>();
    const double *pos = se2state->as<ob::RealVectorStateSpace::StateType>(0)->values;
    const double rot = se2state->as<ob::SO2StateSpace::StateType>(1)->value;
    const double *ctrl = control->as<oc::RealVectorControlSpace::ControlType>()->values;

    result->as<ob::SE2StateSpace::StateType>()->setXY(
            pos[0] + ctrl[0] * duration * cos(rot),
            pos[1] + ctrl[0] * duration * sin(rot));
    result->as<ob::SE2StateSpace::StateType>()->setYaw(
            rot + ctrl[1] * duration);
}

std::vector<geometry_msgs::Twist> velocities;
std::vector<double> velocity_durations;

void set_velocities(const std::vector<oc::Control *> controls) {
    velocities.resize(controls.size());
    for (int i = 0; i < controls.size(); i++) {
        double *values = controls.at(i)->as<ompl::control::RealVectorControlSpace::ControlType>()->values;

        geometry_msgs::Twist velocity;
        velocity.linear.x = values[0];
        velocity.angular.z = values[1];
        velocities.at(i) = velocity;
    }
}

nav_msgs::Path path;

void set_path(const std::vector<ob::State *> &states){
    std::vector<geometry_msgs::PoseStamped> poses;
    poses.resize(states.size());

    std_msgs::Header header;
    header.frame_id = "odom";

    for(int i = 0; i < states.size(); i++){
        geometry_msgs::PoseStamped pose;
//        ob::SE2StateSpace::StateType stateType = *states.at(i)->as<ob::SE2StateSpace::StateType>();
        pose.pose.position.x = states.at(i)->as<ob::SE2StateSpace::StateType>()->getX();
        pose.pose.position.y = states.at(i)->as<ob::SE2StateSpace::StateType>()->getY();
        pose.header = header;
        poses.at(i) = pose;
    }
    path.header = header;
    path.poses = poses;
}

void plan(void) {

    // construct the state space we are planning in
    ob::StateSpacePtr space(new ob::SE2StateSpace());

    // set the bounds for the R^2 part of SE(2)
    ob::RealVectorBounds bounds(2);
    bounds.setLow(-1);
    bounds.setHigh(1);

    space->as<ob::SE2StateSpace>()->setBounds(bounds);

    // create a control space
    oc::ControlSpacePtr cspace(new oc::RealVectorControlSpace(space, 2));

    // set the bounds for the control space
    ob::RealVectorBounds cbounds(2);
    cbounds.setLow(-0.3);
    cbounds.setHigh(0.3);

    cspace->as<oc::RealVectorControlSpace>()->setBounds(cbounds);

    // construct an instance of  space information from this control space
    oc::SpaceInformationPtr si(new oc::SpaceInformation(space, cspace));

    // set state validity checking for this space
    si->setStateValidityChecker(boost::bind(&isStateValid, si.get(), _1));

    // set the state propagation routine
    si->setStatePropagator(boost::bind(&propagate, _1, _2, _3, _4));

    // create a start state
    ob::ScopedState <ob::SE2StateSpace> start(space);
    start->setX(-0.5);
    start->setY(0.0);
    start->setYaw(0.0);

    // create a goal state
    ob::ScopedState <ob::SE2StateSpace> goal(start);
    goal->setX(0.5);

    // create a problem instance
    ob::ProblemDefinitionPtr pdef(new ob::ProblemDefinition(si));

    // set the start and goal states
    pdef->setStartAndGoalStates(start, goal, 0.1);

    // create a planner for the defined space
    //ob::PlannerPtr planner(new oc::RRT(si));
    //ob::PlannerPtr planner(new oc::EST(si));
    //ob::PlannerPtr planner(new oc::KPIECE1(si));
    oc::DecompositionPtr decomp(new MyDecomposition(32, bounds));
    ob::PlannerPtr planner(new oc::SyclopEST(si, decomp));
    //ob::PlannerPtr planner(new oc::SyclopRRT(si, decomp));

    // set the problem we are trying to solve for the planner
    planner->setProblemDefinition(pdef);

    // perform setup steps for the planner
    planner->setup();


    // print the settings for this space
    si->printSettings(std::cout);

    // print the problem settings
    pdef->print(std::cout);

    // attempt to solve the problem within one second of planning time
    ob::PlannerStatus solved = planner->solve(10.0);

    if (solved) {
        // get the goal representation from the problem definition (not the same as the goal state)
        // and inquire about the found path
        ob::PathPtr path = pdef->getSolutionPath();
        std::cout << "Found solution:" << std::endl;
        // print the path to screen
        path->print(std::cout);
    }
    else
        std::cout << "No solution found" << std::endl;
}


void planWithSimpleSetup(void) {
    // construct the state space we are planning in
    ob::StateSpacePtr space(new ob::SE2StateSpace());

    // set the bounds for the R^2 part of SE(2)
    ob::RealVectorBounds bounds(2);
    bounds.setLow(-1);
    bounds.setHigh(1);

    space->as<ob::SE2StateSpace>()->setBounds(bounds);

    // create a control space
    oc::ControlSpacePtr cspace(new oc::RealVectorControlSpace(space, 2));

    // set the bounds for the control space
    ob::RealVectorBounds cbounds(2);
    cbounds.setLow(-0.3);
    cbounds.setHigh(0.3);

    cspace->as<oc::RealVectorControlSpace>()->setBounds(cbounds);

    // define a simple setup class
    oc::SimpleSetup ss(cspace);

    // set the state propagation routine
    ss.setStatePropagator(boost::bind(&propagate, _1, _2, _3, _4));

    // set state validity checking for this space
    ss.setStateValidityChecker(boost::bind(&isStateValid, ss.getSpaceInformation().get(), _1));

    // create a start state
    ob::ScopedState <ob::SE2StateSpace> start(space);
    start->setX(0.0);
    start->setY(0.0);
    start->setYaw(0.0);

    // create a  goal state; use the hard way to set the elements
    ob::ScopedState <ob::SE2StateSpace> goal(space);
    (*goal)[0]->as<ob::RealVectorStateSpace::StateType>()->values[0] = 0.0;
    (*goal)[0]->as<ob::RealVectorStateSpace::StateType>()->values[1] = 3.0;
    (*goal)[1]->as<ob::SO2StateSpace::StateType>()->value = 0.0;


    // set the start and goal states
    ss.setStartAndGoalStates(start, goal, 0.05);

    // ss.setPlanner(ob::PlannerPtr(new oc::PDST(ss.getSpaceInformation())));
    // ss.getSpaceInformation()->setMinMaxControlDuration(1,100);
    // attempt to solve the problem within one second of planning time
    ob::PlannerStatus solved = ss.solve(10.0);

    if (solved) {
        std::cout << "Found solution:" << std::endl;
        // print the path to screen

        oc::PathControl solution = ss.getSolutionPath();
//        velocity_durations = solution.getControlDurations();
//        set_path(solution.getControls());
        set_path(solution.getStates());

//        ss.getSolutionPath().printAsMatrix(std::cout);
    }
    else
        std::cout << "No solution found" << std::endl;
}

int main(int argc, char *argv[]) {
    ros::init(argc, argv, "velocity_publisher");
    ros::NodeHandle nh;

    std::cout << "OMPL version: " << OMPL_VERSION << std::endl;
//    ros::Publisher cmd_publisher = nh.advertise<geometry_msgs::Twist>("/robot0/cmd_vel", 1000);
    ros::Publisher path_publisher = nh.advertise<nav_msgs::Path>("/path", 1000);


//     plan();
    //
    // std::cout << std::endl << std::endl;
    //
    planWithSimpleSetup();

    ROS_WARN_STREAM("publishing path...");
    while(ros::ok()){
        path_publisher.publish(path);
    }

//    for (int i = 0; i < velocities.size(); i++) {
//        cmd_publisher.publish(velocities.at(i));
//        geometry_msgs::Twist command = velocities.at(i);
////        ROS_INFO_STREAM("[" + boost::lexical_cast<std::string>(command.linear.x)
////                        + " | " + boost::lexical_cast<std::string>(command.linear.y)
////                        + "]"
////        );
//        ros::Duration(velocity_durations.at(i)).sleep();
//    }

//    ros::Duration(0.5).sleep();
//    geometry_msgs::Twist stop;
//
//    cmd_publisher.publish(stop);

    return 0;
}